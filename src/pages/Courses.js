import CourseCard from '../components/CourseCard'
import Loading from '../components/Loading'
import {useEffect, useState} from 'react'

export default function Courses(){
  const [courses, setCourses] = useState([])
  const [isLoading, setIsLoading] = useState(true)

  useEffect((isLoading) => {

    // If it detects the data coming from fetch, the set isloading going to be false
    fetch(`${process.env.REACT_APP_API_URL}/courses/`)
    .then(response => response.json())
    .then(result => {
      console.log(result)
      setCourses(
        result.map(course => {
          return (
            <CourseCard key={course._id} courseProp ={course}/>
          )
        })
      )

      // Sets the loading state to false
      setIsLoading(false)
    })
  }, [])



  return(
      
        (isLoading) ?
          <Loading/>
      :
        <>
          {courses}
        </>
  )
}
