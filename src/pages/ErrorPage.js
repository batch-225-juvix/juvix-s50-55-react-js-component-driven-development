// S53 ACTIVITY
import { Link, NavLink } from "react-router-dom";


export default function PageNotFound() {
  return (
    <div>
      <h2></h2>
      <h1>Page Not Found</h1>
      <p>
        Go back to the <Link to="/">homepage</Link>
      </p>
    </div>
  );
}