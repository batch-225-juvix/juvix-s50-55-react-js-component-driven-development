import { useState, useEffect, useContext } from "react";
import { Form, Button } from "react-bootstrap";
import { useNavigate, Navigate } from "react-router-dom";
import UserContext from "../UserContext";
import Swal from "sweetalert2";
import "../App.css";
import "../style.css"
// import RiseLoader from "react-spinners/RiseLoader";
// import image from "../pics/background-login.jpg";

export default function Login() {
  // Initializes the use of the properties from the UserProvider in App.js file
  const { user, setUser } = useContext(UserContext);

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  // Initialize useNavigate
  // const navigate = useNavigate()

  // For determining if button is disabled or not
  const [isActive, setIsActive] = useState(false);
  const [loading, setLoading] = useState(false);
  const navigate = useNavigate();

  const retrieveUser = (token) => {
    setLoading(true);
    fetch(`${process.env.REACT_APP_API_URL}/user/details`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then((response) => response.json())
      .then((result) => {
        // Store the user details retrieved from the token into the global user state
        setUser({
          id: result._id,
          isAdmin: result.isAdmin,
        });
        setLoading(false);
        
          navigate("/products");
      })
      .catch((error) => {
        setLoading(false);
        Swal.fire({
          title: "Oops!",
          icon: "error",
          text: "Something went wrong. Please try again.",
        });
      });
  };

  function authenticate(event) {
    event.preventDefault();

    setLoading(true);
    fetch(`${process.env.REACT_APP_API_URL}/user/login`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email: email,
        password: password,
      }),
    })
      .then((response) => response.json())
      .then((result) => {
        if (typeof result.access !== "undefined") {
          localStorage.setItem("token", result.access);

          retrieveUser(result.access);
          const Toast = Swal.mixin({
            toast: true,
            position: "middle-center",
            showConfirmButton: false,
            timer: 3000,
            timerProgressBar: true,
            didOpen: (toast) => {
              toast.addEventListener("mouseenter", Swal.stopTimer);
              toast.addEventListener("mouseleave", Swal.resumeTimer);
            },
          });

          Toast.fire({
            icon: "success",
            title: "Logged in successfully",
          });
        } else {
          Swal.fire({
            title: "Authentication Failed!",
            icon: "error",
            text: "Invalid Email or password",
          });
        }
      });
  }

  useEffect(() => {
    if (email !== "" && password !== "") {
      // Enables the submit button if the form data has been verified
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [email, password]);

  return user.id !== null ? (
    <Navigate to="/products" />
  ) : (
    <div className="background">
     

      <div className="motherContainer"></div>
      <Form onSubmit={(event) => authenticate(event)}>
        <Form.Group controlId="userEmail">
          <Form.Label id="emailText" className="loginText">
            Email
          </Form.Label>
          <Form.Control
            className="emailBox"
            type="email"
            placeholder="Enter email"
            value={email}
            onChange={(event) => setEmail(event.target.value)}
            required
          />
        
        </Form.Group>

        <p className="loginParagraph">
          Don't have an Account yet? Click <a href="/register">here</a> to
          register
        </p>
        <Form.Group controlId="password">
          <Form.Label id="passText" className="loginText">
            Password
          </Form.Label>
          <Form.Control
            className="passBox"
            type="password"
            placeholder="Password"
            value={password}
            onChange={(event) => setPassword(event.target.value)}
            required
          />
        </Form.Group>
        {isActive ? (
          <div className={`btnTrans ${isActive ? "show" : ""}`}>
            <Button
              variant="transparent"
              type="submit"
              id="submitBtn"
              className="animate__backInRight"
            >
              <div className="btnText">LOGIN</div>
            </Button>
          </div>
        ) : (
          <div className={`btnLoad ${!isActive ? "show" : ""}`}>
            {/* <RiseLoader color="white" size={13} /> */}
          </div>
        )}
      </Form>
    </div>
  );
}



















/* 

import {useState, useEffect, useContext} from 'react'
import {Form, Button} from 'react-bootstrap'
import {useNavigate, Navigate} from 'react-router-dom'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'
import '../style.css'

export default function Login(){
    // Initializes the use of the properties from the UserProvider in App.js file
    const {user, setUser} = useContext(UserContext)

    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')

    // Initialize useNavigate
    // const navigate = useNavigate()

    // For determining if button is disabled or not
    const [isActive, setIsActive] = useState(false)

    const retrieveUser = (token) => {
        fetch(`${process.env.REACT_APP_API_URL}/user/details`, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
        .then(response => response.json())
        .then(result => {

            // Store the user details retrieved from the token into the global user state
            setUser({
                id: result._id,
                isAdmin: result.isAdmin
            })
        })
    }

    function authenticate(event){
        event.preventDefault()
        
        fetch(`${process.env.REACT_APP_API_URL}/user/login`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(response => response.json())
        .then(result => {
            if(typeof result.access !== "undefined"){
                localStorage.setItem('token', result.access)

                retrieveUser(result.access)

                Swal.fire({
                    title: 'Login Successful!',
                    icon: 'success',
                    text: 'Welcome to Zuitt!'
                })
            } else {
                Swal.fire({
                    title: 'Authentication Failed!',
                    icon: 'error',
                    text: 'Invalid Email or password'
                })
            }
        })
    }

    useEffect(() => {
        if((email !== '' && password !== '')){
            // Enables the submit button if the form data has been verified
            setIsActive(true)
        } else {
            setIsActive(false)
        }
    }, [email, password])

    return(
        (user.id !== null) ?
            <Navigate to="/courses"/>
        :  



        
        <div className='background'>
                <div className='motherContainer'>
                    <Form onSubmit={event => authenticate(event)}>

                        <div className='email1'>
                            <Form.Group controlId="userEmail">
                                <Form.Label>Email address</Form.Label>
                                <Form.Control 
                                    type="email" 
                                    placeholder="Enter email"
                                    value={email}
                                    onChange={event => setEmail(event.target.value)}
                                    required
                                />
                                
                                <Form.Text className="text-muted">
                                    We'll never share your email with anyone else.
                                </Form.Text>
                            </Form.Group>
                            


                            <Form.Group controlId="password">
                                <Form.Label>Password</Form.Label>
                                <Form.Control 
                                    type="password" 
                                    placeholder="Password"
                                    value={password}
                                    onChange={event => setPassword(event.target.value)}
                                    required
                                />
                            </Form.Group>
                            </div>

                        
                            <div className='button'>
                                {   isActive ?
                                    <Button variant="primary" type="submit" id="submitBtn">
                                        Submit
                                    </Button>
                                    :
                                    <Button variant="primary" type="submit" id="submitBtn" disabled>
                                        Submit
                                    </Button>
                                } 
                            </div>
                        

                    </Form>
                </div>
        </div>
    )
}


 */























