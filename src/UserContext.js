import React from 'react' // s54 dicussion (3)


// Create a Context object.

// Context object is a different approach in passing information between components and allows easier access by avoiding props-drilling

// Type of object that can be used to store imformation that can be shared to other components within the app. // s54 dicussion (4)
const UserContext = React.createContext() // Base


// The "Provider" component allows other components to consume/use the context object and supply the necessary information need to the context object.
// s54 dicussion (5)
export const UserProvider = UserContext.Provider; // Deliver


export default UserContext